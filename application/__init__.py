from flask import Flask
from flask_heroku import Heroku
from flask_cors import CORS
# For persistent storage
from flask_sqlalchemy import SQLAlchemy
import os

#create the flask app
app = Flask(__name__)
CORS(app)

# load configuration from config.cfg
if "DEPLOY" in os.environ:
    app.config.from_envvar('DEPLOY')
else:
    app.config.from_pyfile('config_dev.cfg')

# instantiate the Heroku object before db
heroku = Heroku(app)
# instantiate SQLAlchemy to handle db process
db = SQLAlchemy(app)


#use the route.py for request routing
from application import routes