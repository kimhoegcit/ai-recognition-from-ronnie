from flask_wtf import FlaskForm
from wtforms import SubmitField, TextField
from wtforms.validators import Length, InputRequired, ValidationError, NumberRange

class LoadModelForm(FlaskForm):
    model_name   = TextField("Model Name", validators=[InputRequired()])
    model_url   = TextField("URL of model", validators=[InputRequired()])
    submit = SubmitField("Save Model")
