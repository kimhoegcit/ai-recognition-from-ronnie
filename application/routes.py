from application import app, db
from application.forms import LoadModelForm
from application.models import Entry
from datetime import datetime

from flask import render_template, request, flash, json, jsonify
from flask_cors import CORS, cross_origin

import os
import re

#create the database if not exist
db.create_all()

#Landing Page
@app.route('/') 
@app.route('/index') 
@app.route('/home') 
def index_page(): 
    form = LoadModelForm()
    return render_template("index.html", 
        title="Object Recognition",
        form=form,entries=get_entries())
#Add Model Submission
@app.route("/addModel", methods=['GET','POST'])
def predict():
    form = LoadModelForm()
    if request.method == 'POST':
        if form.validate_on_submit():
            model_name = form.model_name.data
            model_url = form.model_url.data

            new_entry = Entry(  model_name=model_name, model_url=model_url,
                                saved_on=datetime.utcnow())
            result = add_entry(new_entry)
            flash(f"Result: {result} {model_name} added","success")
        else:
            flash("Error, cannot proceed with saving","danger")
    return render_template("index.html", 
        title="Object Recognition", 
        form=form, entries=get_entries(), index=True)


#Handles http://127.0.0.1:5000/remove
@app.route('/remove', methods=['POST'])
def remove():
    form = LoadModelForm()
    req = request.form
    id = req["id"]
    remove_entry(id)
    return render_template("index.html", 
        title="Object Recognition", 
        form=form, entries = get_entries(), index=True )


# Database Operation
def add_entry(new_entry):
    try:
        db.session.add(new_entry)
        db.session.commit()
        return new_entry.id
    except Exception as error:
        db.session.rollback()
        flash(error,"danger")

def get_entries():
    try:
        entries = Entry.query.all()
        return entries
    except Exception as error:
        db.session.rollback()
        flash(error,"danger") 
        return 0  

def remove_entry(id):
    try:
        entry = Entry.query.get(id)
        db.session.delete(entry)
        db.session.commit()
    except Exception as error:
        db.session.rollback()
        flash(error,"danger")

