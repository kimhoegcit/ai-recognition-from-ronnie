from application import db
import datetime as dt
from sqlalchemy.orm import validates 

class Entry(db.Model):
    __tablename__ = 'Models'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    model_name = db.Column(db.String)
    model_url = db.Column(db.String)
    saved_on = db.Column(db.DateTime, nullable=False)

    @validates('model_name') 
    def validate_model_name(self, key, model_name):
        if len(model_name) <=0:
            raise AssertionError('Value cannot be empty')
        return model_name

    @validates('model_url') 
    def validate_model_url(self, key, model_url):
        if len(model_url) <=0:
            raise AssertionError('Value cannot be empty')
        return model_url
    
